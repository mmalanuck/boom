# -*- coding: utf8 -*-

from compile_me.ast import BaseNode, Node
from compile_me.equality import Equality

__author__ = 'cjay'


class Comment(BaseNode):
    """
    Нода комментария
    """
    state = 3

    def __init__(self, content, pos):
        super(Comment, self).__init__(pos)
        self.content = content

    def get_name(self):
        return "Comment"

    def __repr__(self):
        return '|%d| CommentNode: %s...' % (self.pos, self.content[:10])

    def to_text(self):
        return "// %s" % self.content


class Block(Node):
    """
    Нода блока
    """
    NodeName = "Block"

    def __init__(self, pos, name, mods=None, content=None, props = None ):
        super(Block, self).__init__(name, pos)
        self.mods = mods if mods else {}
        self.content = content if content else []
        self.props= props if props else {}


class Element(Node):
    NodeName = "Element"

    def __init__(self, pos, name, mods=None, content=None, props = None):
        super(Element, self).__init__(name, pos)
        self.mods = mods if mods else {}
        self.content = content if content else []
        self.props = props if props else {}



class Indent(BaseNode):
    def __init__(self, size, pos):
        super(Indent, self).__init__(pos)
        self.size = size

    def __repr__(self):
        return '|%d|  Indent: %s' % (self.pos, self.size)

class ContextName(Equality):
    def __init__(self, name, default=None):
        self.name = name
        self.default_value = default

    def __call__(self, ctxt):
        return ctxt.get(self.name, self.default_value)

    def __repr__(self):
        return '(prop=%s)' % (self.name)

class PropertyMapper(BaseNode):
    def __init__(self, name, value, pos):
        super(PropertyMapper, self).__init__(pos)
        self.name = name
        self.value = value

    def __repr__(self):
        return '|%d| (%s=%s)' % (self.pos, self.name, str(self.value))

class ComponentName(BaseNode):
    def __init__(self, name, pos):
        super(ComponentName, self).__init__(pos)
        self.name = name

    def __repr__(self):
        return '|%d| ComponentName(%s)' % (self.pos, self.name)