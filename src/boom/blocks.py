# -*- coding: utf8 -*-

__author__ = 'cjaym'

import ast

block_dict = {}
elems_dict = {}

def register_block(item, name=None):
    global block_dict
    if not name:
        name = item.__name__
    if not block_dict.has_key(name):
        block_dict[name] = item

def register_element(item, name=None):
    global elems_dict
    if not name:
        name = item.__name__
    if not elems_dict.has_key(name):
        elems_dict[name] = item


class Prop(object):
    """
    Это свойство (props)
    """
    def __init__(self, default_value=None):
        self.value = default_value

    def get(self, ctx = None):
        return self.value


def _property_context_getter(cls_instance, ctxt, key):
    """
    Вытаскивает из props значение из контекста по ключу

    :param cls_instance:
    :param ctxt:
    :param key:
    :return:
    """
    prop = cls_instance._props.get(key, None)
    if prop is None:
        raise Exception("Can`t find props with name \"%s\""%key)
    res = ctxt.get(prop.name, None)
    if res is None:
        res = prop.default_value
    if res is None:
        raise KeyError("Can`t find property name (%s) in context"%prop.name)
    return res

def _property_component_getter(cls_instance, ctxt, key):
    """
    Вытаскивает из props класс компонента

    :param cls_instance:
    :param ctxt:
    :param key:
    :return:
    """
    global block_dict
    prop = cls_instance._props.get(key)
    if not prop:
        raise Exception("Can`t find props with name \"%s\""%key)
    res = block_dict.get(prop.name)
    if not res:
        raise KeyError("Can`t find register component (%s)"%prop.name)
    return res

def make_property_getter(v):
    fnc = None
    if isinstance(v, ast.ContextName):
        fnc = lambda _key: lambda _cls, _ctxt=None: _property_context_getter(_cls, _ctxt, _key)
    elif isinstance(v, ast.ComponentName):
        fnc = lambda _key: lambda _self, _ctxt=None: _property_component_getter(_self, _ctxt, _key)
    else:
        fnc = lambda _key: lambda _self, _ctxt=None: _self._props.get(_key)
    return fnc


class BlockMeta(type):
    def __init__(cls, cls_name,
                cls_parents, cls_attrs):
        super(BlockMeta, cls).__init__(cls)

        props_dict = {}
        setattr(cls, "_props", props_dict)
        _props = cls_attrs.get("props")

        if _props:
            delattr(cls, "props")
            setattr(cls, "_props", _props)
            setattr(cls, "_default_props", _props.copy())

            # Добавляем геттеры свойств
            for k, v in _props.items():
                fnc = make_property_getter(v)
                setattr(cls, k, classmethod(fnc(k)))

        _name = cls_attrs.get("_component_name", None)
        if not _name:
            _name = cls_name
        setattr(cls,"_component_name", _name)


class Component(object):
    __metaclass__ = BlockMeta

    extra_classes = []
    props_default = {}

    def __init__(self, content=None, mods=None, props=None):
        self.content = content if content else []
        self.mods = mods if mods else {}

        if props:
            for k, v in props.items():
                if not self._props.has_key(k):
                    raise KeyError("%s has no such key (%s)" % (self.__class__.__name__, k))
                self._props[k] = v
                fnc = make_property_getter(v)(k)
                fnc = fnc.__get__(self, Component)
                setattr(self, k, fnc)

    def addition_styles(self, ctxt):
        return ""

    def setProps(self, d):
        self.props.update(d)


    def add(self, item):
        self.content.append(item)

    def __call__(self, ctxt):

        content = self.render(ctxt)

        mods = []
        for k, v in self.mods.iteritems():
            mods.append("B-%(name)s_%(key)s_%(value)s"%{"name":self._component_name, "key": k, "value": v})
        mods_string = " ".join(mods)
        style = "B-%s %s"%(self._component_name, self.addition_styles(ctxt))
        if mods_string:
            style = "%s %s" % (style, mods_string)

        return "<div class='%(style)s'>%(content)s</div>" % {
            "style": style,
            "content": content
        }

    def render(self, ctxt):
        class_name = "b-%s" % self.name
        mods_string = ' '.join(["%(name)s_%(k)s_%(value)s"%{
            "name":class_name,
            "k": k,
            "value": v
            }  for k,v in self.mods.iteritems()])

        if mods_string:
            class_name +=" " +mods_string

        if self.extra_classes:
            class_name += " " + " ".join(self.extra_classes)

        content = "\n".join(i(ctxt) for i in self.content)
        res = "<div class='%(name)s' >%(content)s</div>"%{
            "name": class_name,
            "content": content
        }
        return res


class NotFoundComponent(Component):
    def __init__(self, name):
        super(NotFoundComponent, self).__init__()
        self.name = name

    def render(self, context=None):
        return "<div class='B-NotFoundBlock'>Block <span class='B-NotFoundBlock--name'>'%s'</span> not found</div>"%self.name



def make_tree(ast_tree):
    res = []
    for i in ast_tree:
        item = make_blocks(i)
        if item:
            res.append(item)


    return res

def make_blocks(node):
    """
    Рекурсинвый обход ноды с созданием необходимого блока с содержимым
    и модификаторами
    :param node:
    :return:
    """
    item_type = None
    item = None
    if isinstance(node, ast.Block):
        item_type = block_dict.get(node.name)
        if not item_type:
            item = NotFoundComponent(node.name)


    if isinstance(node, ast.Element):
        item_type = elems_dict.get(node.name)


    if item:
        return item

    # Создаём блок
    content = []
    for child in node.content:
        # Если у нас блок
        if isinstance(child, (ast.Block, ast.Element)):
            child_item = make_blocks(child)
            if child_item:
                content.append(child_item)
    item = item_type(content=content, mods=node.mods, props=node.props)

    return item