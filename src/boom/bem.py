# -*- coding: utf8 -*-

__author__ = 'cjaym'


class Bem():
    def __init__(self, content=None):
        self.styles = [
            "/static/css/libs/bootstrap.min.css",
            "/static/css/font-awesome.min.css",
            "/static/css/material-design-iconic-font.min.css",
            "/static/css/pikaday.css",
            "/static/css/bem.css"
        ]
        self.top_plugins = [
            '/static/js/libs/modernizr-2.6.2.min.js'
        ]
        self.bottom_plugins = [

        ]
        self.title = "Title"
        self.description = "description"
        self.content = content if content else []

    def add(self, item):
        self.content.append(item)

    def __call__(self, ctxt):
        return self.render(ctxt)

    def render(self, ctxt):
        styles ="\n".join(['<link href="%s" media="all" rel="stylesheet" type="text/css"/>'%i for i in self.styles])
        top_scripts = "\n".join(['<script src="%s"></script>'%i for i in self.top_plugins])
        bottom_scripts = "\n".join(['<script src="%s"></script>'%i for i in self.bottom_plugins])
        content = "\n".join(i(ctxt) for i in self.content)
        res = """<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    %(styles)s
    %(top_scripts)s

    </head>
<body>


%(content)s
<div id='REACT_RENDER'></div>
%(bottom_scripts)s


</body>
</html>"""%{"styles": styles,
            "top_scripts": top_scripts,
            "bottom_scripts": bottom_scripts,
            "content":content}


        return res


