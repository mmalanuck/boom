# -*- coding: utf8 -*-

__author__ = 'cjay'

import ast
from compile_me.combinators import *
from lexem import *



def keyword(kw):
    """
    Удобная обёртка над классов Reserved

    :param kw:
    :return:
    """
    return Reserved(kw, RESERVED)


TextNode = Tag(TEXT)
EolNode = Tag(ENDLINE)
SpaceNode = Tag(SPACE)
Tab = Tag(TAB)
Digit = Tag(DIGIT)
Zerro = Tag(ZERRO)


def indent_symbol():
    """
    Отступ. Символ табуляции или 4 пробела
    :return: str
    """

    def modify(parsed, pos, offset=0):
        return "\t"

    return Tab | (SpaceNode + SpaceNode + SpaceNode + SpaceNode ^ modify)



def indetations(indent=0):
    """
    непустая последовательность отступов
    indent - текущий уровень отступа
    :return: Indent
    """

    def mod1(parsed, pos, offset=0):
        result = ast.Indent(1, pos + offset)
        return result

    def mod2(parsed, pos, offset=0):
        first, items = parsed
        result = ast.Indent(len(items) + 1, pos + offset)
        return result

    def modify(parsed, pos, offset=0):
        result = parsed
        if result:
            if result.size < indent:
                return None
        return result

    return Alternate(
        indent_symbol() + Rep(indent_symbol()) ^ mod2,
        indent_symbol() ^ mod1
    ) ^ modify


def digit_and_zerro():
    return Digit | Zerro


def number_uint():
    """
    Возвращает целое положительное число
    :return:
    """

    def root_mod(parsed, pos, offset=0):
        return int(parsed)

    def mod1(parsed, pos, offset=0):
        first, items = parsed
        res = first + "".join(items)
        return res

    return Alternate(
        Digit + Rep(digit_and_zerro()) ^ mod1,
        digit_and_zerro()
    ) ^ root_mod


def number_int():
    def modify(parsed, pos, offset=0):
        _, value = parsed
        return -value

    return Alternate(
        keyword("-") + number_uint() ^ modify,
        number_uint()
    )


def number_unsigned():
    """
    беззнаковое целое или вещественное число
    :return: float
    """

    def modify(parsed, pos, offset=0):
        ((((l_first, l_items), _), r_first), r_items) = parsed
        res = l_first + "".join(l_items) + "." + r_first + "".join(r_items)
        return float(res)

    return Alternate(
        Opt(digit_and_zerro() + Rep(digit_and_zerro())) + keyword(".") + digit_and_zerro() + Rep(
            digit_and_zerro()) ^ modify,
        number_uint()
    )


def number():
    """
    знаковое целое или вещественное число
    :return: float
    """

    def modify(parsed, pos, offset=0):
        _, value = parsed
        return -value

    return Alternate(
        keyword("-") + number_unsigned() ^ modify,
        number_unsigned()
    )


def string_value():
    """
    строка текста
    :return:
    """

    def modify(parsed, pos, offset=0):
        ((_, value), _) = parsed
        res = "".join(value)
        return res

    line = TextNode | Digit | Zerro | keyword("/") | SpaceNode | keyword("-") | keyword("(") | keyword(")") | keyword(",") | keyword(".") | keyword("@") | keyword("=") | keyword("this")

    return Alternate(
        keyword("'") + Rep(line | keyword('"')) + keyword("'"),
        keyword('"') + Rep(line | keyword("'")) + keyword('"')
    ) ^ modify


def value():
    """
    возможное значение модификатора или ещё чего-то
    :return:
    """
    return string_value() | number()


def modifers():
    """
    Описание модификаторов ключ-значение
    :return:
    """

    def mod1(parsed, pos, offset=0):
        (((name_left, _), val_left), items) = parsed
        res = {name_left: val_left}
        for i in items:
            (((_, name), _), val) = i
            res[name] = val
        return res

    def mod2(parsed, pos, offset=0):
        ((name, _), val) = parsed
        return {name: val}

    def reduce_to_eql(parsed, pos, offset = 0):
        return "="

    def reduce_to_comma(parsed, pos, offset = 0):
        return ","

    def mymod(parsed, pos, offset=0):
        ((name, _), val) =  parsed
        return {name: val}

    eq_with_spaces = Rep(SpaceNode) + keyword("=")+Rep(SpaceNode) ^ reduce_to_eql

    def mod_1(parsed, pos, offset=0):
        first_dict, items = parsed
        for i in items:
            (_, loop_dict) = i
            first_dict.update(loop_dict)
        return first_dict

    def mod_text(parsed, pos, offset=0):
        first_str, items = parsed
        first_str += "".join(items)
        return first_str

    single_part = ((TextNode + Rep(TextNode | keyword("-")) ^ mod_text )  + eq_with_spaces  + value()  ^ mymod)
    return  (single_part + Rep((Rep(SpaceNode) + keyword(",") + Rep(SpaceNode))+ single_part ) ^ mod_1) | single_part


def block():
    """
    объявление блока
    :return:
    """

    def mod1(parsed, pos, offset=0):
        (((((name, _), dict), _), _), content) = parsed
        return (name, dict, content)

    def mod2(parsed, pos, offset=0):
        (((name, _), mods), _) = parsed
        return (name, mods, None)

    def mod3(parsed, pos, offset=0):
        ((((name, _), _), _), content) = parsed
        return (name, None, content)

    def mod4(parsed, pos, offset=0):
        ((name, _), _) = parsed
        return (name, None, None)

    def mod5(parsed, pos, offset=0):
        ((name, _), content) = parsed
        return (name, None, content)

    def mod6(parsed, pos, offset=0):
        return (parsed, None, None)

    def modify(parsed, pos, offset=0):
        name, mods, content = parsed
        return ast.Block(pos + offset, name, mods, content)


    return Alternate(
        Alternate(
            Alternate(
                Alternate(
                    Alternate(
                        TextNode + keyword("(") + modifers() + keyword(")") + SpaceNode + value() ^ mod1,
                        TextNode + keyword("(") + modifers() + keyword(")") ^ mod2
                    ),
                    TextNode + keyword("(") + keyword(")") + SpaceNode + value() ^ mod3
                ),
                TextNode + keyword("(") + keyword(")") ^ mod4
            ),
            TextNode + SpaceNode + value() ^ mod5
        ),
        TextNode ^ mod6
    ) ^ modify


def element():
    """
    объявление элемента
    :return:
    """

    def mod1(parsed, pos, offset=0):
        ((((((_, name), _), mods), _), _), content) = parsed
        return (name, mods, content)

    def mod2(parsed, pos, offset=0):
        ((((_, name), _), mods), _) = parsed
        return (name, mods, None)

    def mod3(parsed, pos, offset=0):
        (((((_, name), _), _), _), content) = parsed
        return (name, None, content)

    def mod4(parsed, pos, offset=0):
        (((_, name), _), _) = parsed
        return (name, None, None)

    def mod5(parsed, pos, offset=0):
        (((_, name), _), content) = parsed
        return (name, None, content)

    def mod6(parsed, pos, offset=0):
        (_, name) = parsed
        return (name, None, None)

    def modify(parsed, pos, offset=0):
        name, mods, content = parsed
        return ast.Element(pos + offset, name, mods, content)


    return Alternate(
        Alternate(
            Alternate(
                Alternate(
                    Alternate(
                        keyword("-") + TextNode + keyword("(") + modifers() + keyword(")") + SpaceNode + value() ^ mod1,
                        keyword("-") + TextNode + keyword("(") + modifers() + keyword(")") ^ mod2
                    ),
                    keyword("-") + TextNode + keyword("(") + keyword(")") + SpaceNode + value() ^ mod3
                ),
                keyword("-") + TextNode + keyword("(") + keyword(")") ^ mod4
            ),
            keyword("-") + TextNode + SpaceNode + value() ^ mod5
        ),
        keyword("-") + TextNode ^ mod6
    ) ^ modify


def mapping():
    def mod_value(parsed, pos, offset=0):
        _, name = parsed
        return ast.ContextName(name)

    def modify(parsed, pos, offset=0):
        ((((((_, _), name), _), _), _), value) = parsed
        return ast.PropertyMapper(name, value, pos+offset)

    def mod_blockname(parsed, pos, offset=0):
        name = parsed
        return ast.ComponentName(name, pos+offset)

    return keyword("this") + keyword(".") + TextNode + Rep(SpaceNode | Tab) + keyword("=") + Rep(
        SpaceNode | Tab) + ( keyword("@") + TextNode ^ mod_value | value() | block())^ modify


def all_elements():
    """
    Все конструкции первого прохода
    :return:
    """

    def modify(parsed, pos, offset=0):
        (indent,  res), spaces = parsed
        if not indent:
            indent = ast.Indent(0, pos)
        return indent, res

    return Opt(indetations()) + Alternate(
        block(),
        Alternate(
            element(),
            mapping()
        )
    ) + Rep(SpaceNode | Tab) ^ modify




def second_pass(first_pass):
    """
    Вторичная обработка
    :param first_pass: Result
    :return:
    """

    last_indent = 0
    current_obj = None

    items = None

    root_items = []
    root_indent = 0

    parent_items = root_items
    last_indent = root_indent

    stack = [(root_indent, root_items, None)]

    last_node = None

    for i in first_pass:
        indent = i[0].size
        node = i[1]

        if indent > last_indent:
            stack.append((last_indent, parent_items, last_node))
            parent_items = last_node.content
            last_indent = indent
        elif indent < last_indent:
            if len(stack) == 0:
                raise Exception("No items in stack")

            last_indent, parent_items, last_node = stack.pop()
            while indent < last_indent:
                if len(stack) == 0:
                    parent_items = root_items
                    last_indent = root_indent
                    break
                last_indent, parent_items, last_node = stack.pop()
            if indent != last_indent:
                raise Exception("Indentation error")

        if indent == last_indent:

            if isinstance(node, (ast.Block, ast.Element)):
                parent_items.append(node)
            elif isinstance(node, ast.PropertyMapper):
                last_node.props[node.name] = node.value


        if isinstance(node, (ast.Element, ast.Block)):
            last_node = node

    return root_items


def root():
    def modify(parsed, pos, offset=0):
        res = []
        items, last = parsed
        for i in items:
            ((indent, node), _) = i
            res.append((indent, node))
        if last:
            res.append(last)
        return res

    return Rep(all_elements() + EolNode) + Opt(all_elements()) ^ modify


def compile_me(code):
    """
    использовать этот метод для перевода кода в структуру блоков/элементов
    :param code:
    :return:
    """
    if not code:
        return []
    tokens = lex_analyzer(code)
    parser = root()
    result = parser(tokens, 0)
    return second_pass(result.value)