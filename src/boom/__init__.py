# -*- coding: utf8 -*-

__author__ = 'cjaym'

from blocks import Component, make_tree
from grammar import second_pass, compile_me
from bem import Bem

template_dir = "D:\\develop\\hstutors\\flip_flask\\hs\\boom_templates\\"

def render_string(code,  ctxt=None, styles=None, scripts=None):
    res = compile_me(code)
    bem = Bem(make_tree(res))
    if styles:
        for i in styles:
            if i not in bem.styles:
                bem.styles.append(i)
    if scripts:
        for i in scripts:
            if i not in bem.bottom_plugins:
                bem.bottom_plugins.append(i)
    return bem(ctxt)

def render_template(name, ctxt=None, styles=None, scripts=None):
    with open(template_dir+name) as file:
        code = file.read()
        return render_string(code, ctxt, styles, scripts)


