# -*- coding: utf8 -*-

from lexem import lex_analyzer

__author__ = 'cjaym'

import unittest


class TokensTestCase(unittest.TestCase):

    def setUp(self):
        super(TokensTestCase, self).setUp()
        pass

    def tearDown(self):
        super(TokensTestCase, self).tearDown()
        pass


    def test_lex_tokens(self):
        filepath = "D:\\develop\\boom\\src\\boom\\tests\\source.bem"

        tokens = None
        with open(filepath) as file:
            tokens = lex_analyzer(file.read())
        self.assertIsNotNone(tokens)


if __name__ == '__main__':
    unittest.main()