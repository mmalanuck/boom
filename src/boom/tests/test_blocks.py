# -*- coding:utf-8 -*-
from ast import PropertyMapper, ContextName

from grammar import compile_me
from blocks import make_tree, Component, NotFoundComponent, register_block, Prop

__author__ = 'cjaym'


import unittest


class TestComponents(unittest.TestCase):
    def test_name(self):
        """
        Проверяем имя класса
        :return:
        """
        # Имя класса берётся из кода, если не указано статичное поле name
        self.assertEqual(Component._component_name, "Component")

        class OtherComponent(Component):
            pass
        self.assertEqual("OtherComponent", OtherComponent._component_name)

        class SecondComponent(OtherComponent):
            _component_name = "MySecondComponent"

        self.assertEqual("MySecondComponent", SecondComponent._component_name)

    def test_props_default(self):
        """
        Тестируем свойства
        :return:
        """

        # создаётся словарь _props
        class MyComponent(Component):
            pass
        self.assertDictEqual(MyComponent._props, {})

        class MyComponent2(Component):
            props = {
                "prop1": 1
            }
        # Свойство props отсутствует
        self.assertFalse(hasattr(MyComponent2, "props"))

        class MyComponent3(Component):
            props = {
                "key": "value",
                "key2": 10,
                "key3": ContextName("name1")
            }

        # проверяем, что props ушли в _props
        self.assertFalse(hasattr(MyComponent3, "props"))
        self.assertTrue(hasattr(MyComponent3, "_props"))
        self.assertEqual(len(MyComponent3._props.items()), 3 )

        # проверяем, что есть default_props
        self.assertTrue(hasattr(MyComponent3, "_default_props"))
        self.assertEqual(3, len(MyComponent3._default_props.items()))

        # проверяем наличие геттера
        a = MyComponent3()

        self.assertTrue(hasattr(a, "key"))
        self.assertTrue(hasattr(a, "key2"))
        self.assertTrue(hasattr(a, "key3"))

        # проверяем значение props
        self.assertEqual("value", a.key())
        self.assertEqual(10, a.key2())
        ctxt = {"name1":"super"}
        self.assertEqual("super", a.key3(ctxt))

        # если не передали контекст, вылетает ошибка
        self.assertRaises(Exception,lambda :a.key3())

        # если нет ключа, вылетает ошибка
        self.assertRaises(KeyError,lambda :a.key3({"missed":"missed_key"}))



    def test_attribute_access(self):
        class MyComponent(Component):
            props = {"key": "val1", "key2": 'val2'}

        self.assertTrue(hasattr(MyComponent, "key"))
        a = MyComponent()
        self.assertEqual("val1", a.key())
        self.assertEqual("val2", a.key2())

        # Переопределяем значения props в конструкторе
        b = MyComponent(props={"key": "boo", "key2": 3})

        self.assertEqual("boo", b.key())
        self.assertEqual(3, b.key2())

    def test_get_props(self):
        """
        Тестируем возможность получать значение таким синтаксисом:
        a.p_1.get(ctxt)

        :return:
        """
        class MyComponent(Component):
            props = {
                "p_1": 10,
                "p_2": 20
            }

        ctxt = {"prop_1": "this_is_a_value"}
        a = MyComponent(props={"p_1": ContextName("prop_1"), "p_2":20})
        self.assertEqual("this_is_a_value", a.p_1(ctxt))
        self.assertEqual(20, a.p_2(ctxt))

    def test_props(self):
        class MyPropsComponent(Component):
            prop = Prop(10)

