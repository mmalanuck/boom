# -*- coding: utf-8 -*-


import ast
import grammar as gram
from lexem import lex_analyzer

from compile_me.combinators import Result

__author__ = 'cjaym'

import unittest


class GrammarTestCase(unittest.TestCase):
    def check_result(self, parser, code, expected):
        tokens = lex_analyzer(code)
        self.assertIsNotNone(tokens)
        result = parser(tokens, 0)
        self.assertIsNotNone(result)
        self.assertEqual(type(result), Result)
        self.assertEqual(expected, result.value)
        return result

    def test_indent_symbol(self):
        parser = gram.indent_symbol()
        expected = "\t"

        code = " " * 4
        self.check_result(parser, code, expected)

        code = "\t"
        self.check_result(parser, code, expected)

    def test_indent_tabs(self):
        code = "\t"
        parser = gram.indetations()
        expected = ast.Indent(1, 0)
        self.check_result(parser, code, expected)

        code = "\t\t"
        expected = ast.Indent(2, 0)
        self.check_result(parser, code, expected)
        code = "\t\t\t\t"
        expected = ast.Indent(4, 0)
        self.check_result(parser, code, expected)


    def test_indent_spaces(self):
        code = " " * 4
        parser = gram.indetations()
        expected = ast.Indent(1, 0)
        self.check_result(parser, code, expected)

        code = " " * 4 * 2
        expected = ast.Indent(2, 0)
        self.check_result(parser, code, expected)
        code = " " * 4 * 4
        expected = ast.Indent(4, 0)
        self.check_result(parser, code, expected)

    def test_number_uint(self):
        parser = gram.number_uint()

        items = [1, 0, 22, 999, 8125]
        for i in items:
            self.check_result(parser, str(i), i)

    def test_number_int(self):
        parser = gram.number_int()

        items = [1, 0, 22, 999, 8125,
                 -1, -55, -84, 300, -9999]
        for i in items:
            self.check_result(parser, str(i), i)

    def test_number_unsigned(self):
        parser = gram.number_unsigned()

        items = [9, 21, 0.33, 0.999, 4.58, .99, .0001]
        for i in items:
            self.check_result(parser, str(i), i)

    def test_number(self):
        parser = gram.number()

        items = [9, 21, 0.33, 0.999, 4.58, .99, .0001,
                 -9, -21, -0.33, -0.999, -4.58, -.99, -.0001]
        for i in items:
            self.check_result(parser, str(i), i)

    def test_string_value(self):
        parser = gram.string_value()
        code = "'hello'"
        expected = 'hello'
        self.check_result(parser,code, expected)

        code = '"hello"'
        self.check_result(parser,code, expected)

        self.check_result(parser,'"5hello"', "5hello")
        self.check_result(parser,"'/'", "/")
        self.check_result(parser,"'a a'", "a a")
        self.check_result(parser,"'a-a'", "a-a")
        self.check_result(parser,"'a=a'", "a=a")
        self.check_result(parser,"'a(b)'", "a(b)")
        self.check_result(parser,"'a(b,c)'", "a(b,c)")
        self.check_result(parser,"'a(b.c)'", "a(b.c)")
        self.check_result(parser,"'a(b@c)'", "a(b@c)")
        self.check_result(parser,"""'a("this")'""", 'a("this")')

    def test_modifers(self):
        parser = gram.modifers()
        code = "hello=10"
        tokens = lex_analyzer(code)
        res = parser(tokens, 0)
        self.assertEqual(type(res.value), dict)
        self.assertTrue(res.value.has_key("hello"))
        self.assertEqual(res.value["hello"], 10)


        code = "hello=10,name='Misha',phone='zooo', age=28,frac=0.85"
        tokens = lex_analyzer(code)
        res = parser(tokens, 0)
        self.assertEqual(type(res.value), dict)
        self.assertTrue(res.value.has_key("hello"))
        self.assertEqual(res.value["hello"], 10)
        self.assertTrue(res.value.has_key("name"))
        self.assertEqual(res.value["name"], "Misha")
        self.assertTrue(res.value.has_key("phone"))
        self.assertEqual(res.value["phone"], "zooo")
        self.assertTrue(res.value.has_key("age"))
        self.assertEqual(res.value["age"], 28)
        self.assertTrue(res.value.has_key("frac"))
        self.assertEqual(res.value["frac"], 0.85)

        code = "hello='t10'"
        tokens = lex_analyzer(code)
        res = parser(tokens, 0)
        self.assertEqual(type(res.value), dict)
        self.assertTrue(res.value.has_key("hello"))
        self.assertEqual(res.value["hello"], 't10')

        code = "hello='10'"
        tokens = lex_analyzer(code)
        res = parser(tokens, 0)
        self.assertEqual(type(res.value), dict)
        self.assertTrue(res.value.has_key("hello"))
        self.assertEqual(res.value["hello"], '10')

        code = "hello-man='10'"
        tokens = lex_analyzer(code)
        res = parser(tokens, 0)
        self.assertEqual(type(res.value), dict)
        self.assertTrue(res.value.has_key("hello-man"))
        self.assertEqual(res.value["hello-man"], '10')

    def test_block(self):
        parser = gram.block()
        code = "MainBlock"
        expected = ast.Block(0, 'MainBlock')
        self.check_result(parser, code, expected)

        code = "MainBlock()"
        self.check_result(parser, code, expected)

        code = "MainBlock 'Hello'"
        expected = ast.Block(0, 'MainBlock', None, "Hello")
        self.check_result(parser, code, expected)
        tokens = lex_analyzer(code)
        res = parser(tokens, 0)
        self.assertEqual(res.value.mods, {})

        code = "MainBlock() 'Hello'"
        expected = ast.Block(0, 'MainBlock', None, "Hello")
        self.check_result(parser, code, expected)
        tokens = lex_analyzer(code)
        res = parser(tokens, 0)
        self.assertEqual(res.value.content, "Hello")

        code = "MainBlock(name=12,prop2='txt')"
        tokens = lex_analyzer(code)
        res = parser(tokens, 0)


        mods = res.value.mods
        self.assertEqual(type(res.value), ast.Block)
        self.assertTrue(mods.has_key("name"))
        self.assertEqual(mods["name"], 12)
        self.assertTrue(mods.has_key("prop2"))
        self.assertEqual(mods["prop2"], "txt")
        self.assertEqual(res.value.content, [])

        code = "MainBlock(name=12,prop2='txt') 'Hello'"
        tokens = lex_analyzer(code)
        res = parser(tokens, 0)

        mods = res.value.mods
        self.assertEqual(type(res.value), ast.Block)
        self.assertTrue(mods.has_key("name"))
        self.assertEqual(mods["name"], 12)
        self.assertTrue(mods.has_key("prop2"))
        self.assertEqual(mods["prop2"], "txt")
        self.assertEqual(res.value.content, "Hello")

        code = "col(md=8,c='hello' ,d='beee')"
        tokens = lex_analyzer(code)
        res = parser(tokens, 0)
        mods = res.value.mods
        # print res.value.mods
        self.assertEqual(type(res.value), ast.Block)
        self.assertTrue(mods.has_key("md"))
        self.assertEqual(mods["md"], 8)
        self.assertTrue(mods.has_key("c"))
        self.assertEqual(mods["c"], "hello")
        self.assertTrue(mods.has_key("d"))
        self.assertEqual(mods["d"], "beee")
        self.assertEqual(res.value.content, [])




    def test_element(self):
        parser = gram.element()
        code = "-SampleElement"
        expected = ast.Element(0, 'SampleElement')
        self.check_result(parser, code, expected)

        code = "-SampleElement()"
        self.check_result(parser, code, expected)

        code = "-SampleElement 'Hello'"
        expected = ast.Element(0, 'SampleElement', None, "Hello")
        self.check_result(parser, code, expected)
        tokens = lex_analyzer(code)
        res = parser(tokens, 0)
        self.assertEqual(res.value.mods, {})

        code = "-SampleElement() 'Hello'"
        expected = ast.Element(0, 'SampleElement', None, "Hello")
        self.check_result(parser, code, expected)
        tokens = lex_analyzer(code)
        res = parser(tokens, 0)
        self.assertEqual(res.value.content, "Hello")

        code = "-SampleElement(name=12,prop2='txt')"
        tokens = lex_analyzer(code)
        res = parser(tokens, 0)


        mods = res.value.mods
        self.assertEqual(type(res.value), ast.Element)
        self.assertTrue(mods.has_key("name"))
        self.assertEqual(mods["name"], 12)
        self.assertTrue(mods.has_key("prop2"))
        self.assertEqual(mods["prop2"], "txt")
        self.assertEqual(res.value.content, [])

        code = "-SampleElement(name=12,prop2='txt') 'Hello'"
        tokens = lex_analyzer(code)
        res = parser(tokens, 0)

        mods = res.value.mods

        self.assertEqual(type(res.value), ast.Element)
        self.assertTrue(mods.has_key("name"))
        self.assertEqual(mods["name"], 12)
        self.assertTrue(mods.has_key("prop2"))
        self.assertEqual(mods["prop2"], "txt")
        self.assertEqual(res.value.content, "Hello")


    def test_mapping(self):
        code = "this.name = 20"
        parser = gram.mapping()
        expected = ast.PropertyMapper('name', 20, 0)
        self.check_result(parser,code, expected)

        code = "this.name = @good"
        parser = gram.mapping()
        tokens = lex_analyzer(code)
        res = parser(tokens,0)
        expected = ast.PropertyMapper('name', ast.ContextName("good"), 0)
        self.check_result(parser,code, expected)


        # Принимаем название компонента
        code = "this.comp = Component()"
        parser = gram.mapping()
        expected = ast.PropertyMapper("comp", ast.Block(12, 'Component'), 0)
        self.check_result(parser,code, expected)

        code = "this.comp = Component(md=3, cp=8)"
        parser = gram.mapping()
        expected = ast.PropertyMapper("comp", ast.Block(12, 'Component', mods={"md": 3, "cp": 8}), 0)
        self.check_result(parser,code, expected)


    def test_all_elements(self):
        code = "hello()"
        parser = gram.all_elements()
        expected = (ast.Indent(0, 0), ast.Block(0,"hello"))
        self.check_result(parser, code, expected)

        code = "-hello"
        expected = (ast.Indent(0, 0), ast.Element(0,"hello"))
        self.check_result(parser, code, expected)

        code = "this.val=40"
        expected = (ast.Indent(0, 0), ast.PropertyMapper("val", 40, 0))
        self.check_result(parser, code, expected)

        code = "\t\thello()"
        parser = gram.all_elements()
        expected = (ast.Indent(2, 0), ast.Block(2,"hello"))
        self.check_result(parser, code, expected)

        code = "        -hello"
        expected = (ast.Indent(2, 0), ast.Element(8,"hello"))
        self.check_result(parser, code, expected)

        code = "\t\tthis.val=40"
        expected = (ast.Indent(2, 0), ast.PropertyMapper("val", 40, 2))
        self.check_result(parser, code, expected)

        tokens = lex_analyzer(code)
        res = parser(tokens, 0)

    def test_root(self):
        parser = gram.root()
        code = "block()\n\t-elm1\nblock2()"
        expexted = [
            (ast.Indent(0,0),ast.Block(0,"block")),
            (ast.Indent(1,8),ast.Element(9,"elm1")),
            (ast.Indent(0,15),ast.Block(15,"block2")),
        ]
        self.check_result(parser,code, expexted)


        code = "page\n\theader4me\n\t\trow\n\t\t\tcol(md=4)"
        tokens = lex_analyzer(code)
        result = parser(tokens, 0)
        # print "--->", result


    def test_second_pass(self):
        parser = gram.root()
        code = "block()\n\t-elm1\nblock2()"
        tokens = lex_analyzer(code)
        result = parser(tokens, 0)

        expected = [ast.Block(0,"block",content=[ast.Element(9,"elm1")]),
                    ast.Block(15,"block2")]

        res = gram.second_pass(result.value)
        self.assertEqual(res, expected)

    def test_second_pass(self):
        parser = gram.root()
        code = "block()\n\t-elm1\nblock2()"
        tokens = lex_analyzer(code)
        result = parser(tokens, 0)

        expected = [ast.Block(0,"block",content=[ast.Element(9,"elm1")]),
                    ast.Block(15,"block2")]

        res = gram.second_pass(result.value)
        self.assertEqual(res, expected)

    def test_sample(self):
        # TODO: Доделать этот тест
        code = """page\n\theader4me\n\t\trow\n\t\t\tcol(md=4)\n\t\t\t\tlogo\n\t\t\tcol(md=8, c="hello", d='beee')\n\t\t\t\trow\n\t\t\t\t\tcol(md=4, offset=2)\n\t\t\t\t\t\tChageCity\n\t\t\t\t\tcol(md=4)\n\t\t\t\t\t\tPhoneInfo\n\t\t\t\t\tcol(md=4)\n\t\t\t\t\t\tLoginPanel"""
        parser = gram.root()
        tokens = lex_analyzer(code)
        result = parser(tokens, 0)
        # print result


    def test_real_source_file(self):
        with open("boom/tests/source.bem", "r") as file:
            code = file.read()
            res = gram.compile_me(code)



class TestSecondPass(unittest.TestCase):
    def test_main(self):
        # Проверяем на пустой коллекции
        first_pass = []
        res = gram.second_pass(first_pass)
        self.assertEqual(res, [])

        # Проверяем на вложенность на 1 уровень
        first_pass = [
            (ast.Indent(0,0), ast.Block(0, "b1")),
            (ast.Indent(1,1), ast.Block(1, "b2")),
            (ast.Indent(1,2), ast.Element(2, "e2")),
        ]
        res = gram.second_pass(first_pass)
        expected = [
            ast.Block(0,"b1", content=[
                ast.Block(1, "b2"),
                ast.Element(2, "e2"),
            ])
        ]
        self.assertEqual(res, expected)


        # Проверяем на вложенность на 2 уровень
        first_pass = [
            (ast.Indent(0,0), ast.Block(0, "b1")),
            (ast.Indent(1,1), ast.Block(1, "b2")),
            (ast.Indent(2,2), ast.Element(2, "e2")),
        ]
        res = gram.second_pass(first_pass)
        expected = [
            ast.Block(0,"b1", content=[
                ast.Block(1, "b2", content=[ast.Element(2, "e2")])
            ])
        ]
        self.assertEqual(res, expected)

        # Проверяем на вложенность на 3 уровень
        first_pass = [
            (ast.Indent(0,0), ast.Block(0, "b1")),
            (ast.Indent(1,1), ast.Block(1, "b2")),
            (ast.Indent(2,2), ast.Element(2, "e2")),
            (ast.Indent(2,3), ast.Block(3, "b3")),
            (ast.Indent(3,4), ast.Element(4, "e4")),
        ]
        res = gram.second_pass(first_pass)
        expected = [
            ast.Block(0,"b1", content=[
                ast.Block(1, "b2", content=[
                    ast.Element(2, "e2"),
                    ast.Block(3, "b3", content=[
                        ast.Element(4, "e4")
                    ])
                ])
            ])
        ]
        self.assertEqual(res, expected)

        #Проверяем шаг вглубь и назад
        first_pass = [
            (ast.Indent(0,0), ast.Block(0, "b1")),
            (ast.Indent(1,1), ast.Block(1, "b2")),
            (ast.Indent(2,2), ast.Element(2, "e2")),
            (ast.Indent(1,3), ast.Block(3, "b3")),
            (ast.Indent(2,4), ast.Element(4, "e4")),
        ]
        res = gram.second_pass(first_pass)
        expected = [
            ast.Block(0,"b1", content=[
                ast.Block(1, "b2", content=[
                    ast.Element(2, "e2")
                ]),
                ast.Block(3, "b3", content=[
                    ast.Element(4, "e4")
                ])
            ])
        ]
        self.assertEqual(res, expected)

        #Проверяем шаг вглубь и назад на 2 шага
        first_pass = [
            (ast.Indent(0,0), ast.Block(0, "b1")),
                (ast.Indent(1,1), ast.Block(1, "b2")),
                    (ast.Indent(2,1), ast.Block(2, "b3")),
                        (ast.Indent(3,2), ast.Element(2, "e2")),
            (ast.Indent(0,3), ast.Block(3, "b4")),
                (ast.Indent(1,4), ast.Element(4, "e4")),
        ]
        res = gram.second_pass(first_pass)
        expected = [
            ast.Block(0,"b1", content=[
                ast.Block(1, "b2", content=[
                    ast.Block(2, "b3", content=[
                        ast.Element(2, "e2")
                    ])
                ]),
            ]),
            ast.Block(3, "b4", content=[
                ast.Element(4, "e4")
            ])
        ]
        self.assertEqual(res, expected)

        #Проверяем шаг вглубь и назад на 2 шага и вперёд на 2 шага
        first_pass = [
            (ast.Indent(0,0), ast.Block(0, "b1")),
                (ast.Indent(1,1), ast.Block(1, "b2")),
                    (ast.Indent(2,1), ast.Block(2, "b3")),
                        (ast.Indent(3,2), ast.Element(2, "e2")),
            (ast.Indent(0,3), ast.Block(3, "b4")),
                    (ast.Indent(2,4), ast.Element(4, "e4")),
        ]
        res = gram.second_pass(first_pass)
        expected = [
            ast.Block(0,"b1", content=[
                ast.Block(1, "b2", content=[
                    ast.Block(2, "b3", content=[
                        ast.Element(2, "e2")
                    ])
                ]),
            ]),
            ast.Block(3, "b4", content=[
                ast.Element(4, "e4")
            ])
        ]
        self.assertEqual(res, expected)

        #Проверяем вложенность в Element
        first_pass = [
            (ast.Indent(0,0), ast.Block(0, "b1")),
                (ast.Indent(1,1), ast.Block(1, "b2")),
                    (ast.Indent(2,1), ast.Element(2, "e2")),
                        (ast.Indent(3,2), ast.PropertyMapper("name1", "hello1", 4)),
                        (ast.Indent(3,2), ast.PropertyMapper("name2", "hello2", 4)),
        ]
        res = gram.second_pass(first_pass)
        expected = [
            ast.Block(0,"b1", content=[
                ast.Block(1, "b2", content=[
                    ast.Element(2, "e2", props={
                        "name1":"hello1",
                        "name2":"hello2"
                    } )
                ]),
            ])

        ]

        # A = res[0].content[0].content[0]
        # B = expected[0].content[0].content[0]
        # print("A =", A.props)
        # print("B =", B.props)
        # print("A == B", A==B)

        self.assertEqual(res, expected)



