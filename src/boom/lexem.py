# -*- coding: utf8 -*-

from compile_me import lexer

__author__ = 'cjaym'


RESERVED = 'RESERVED'   # любое зарезервированное слово
ENDLINE = "ENDLINE"     # конец строки
SPACE = "SPACE"         # пустой пробел
TAB = "TAB"             # символ табуляции
DIGIT = "DIGIT"         # цифры от 1 до 9
ZERRO = "ZERRO"         # символ нуля (0)
TEXT = 'TEXT'           # текстовая строка

token_exprs = [
    (r'/', RESERVED),
    (r'\t', TAB),
    (r'\n', ENDLINE),
    (" ", SPACE),
    (r'-', RESERVED),
    (r'\(', RESERVED),
    (r'\)', RESERVED),
    (r',', RESERVED),
    (r'\.', RESERVED),
    (r'@', RESERVED),
    (r'"', RESERVED),
    (r"'", RESERVED),
    (r'=', RESERVED),
    (r'[1-9]', DIGIT),
    (r'0', ZERRO),
    (r'this', RESERVED),
    (r'[^ \n\t=\(\),\."\'-]+', TEXT)
]


def lex_analyzer(characters):
    return lexer.lex_me(characters, token_exprs)